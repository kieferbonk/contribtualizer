import babel from 'rollup-plugin-babel';
import eslint from 'rollup-plugin-eslint';
import replace from 'rollup-plugin-replace';
import uglify from 'rollup-plugin-uglify';
import postcss from 'rollup-plugin-postcss';
import postcssModules from 'postcss-modules';

import cssimport from 'postcss-import';
import simplevars from 'postcss-simple-vars';
import nested from 'postcss-nested';
import cssnext from 'postcss-cssnext';
import cssnano from 'cssnano';

const cssExportMap = {};

const pkg = require('./package.json');

const external = Object.keys(pkg.dependencies);

export default {
    entry: 'src/scripts/main.js',
    dest: 'build/js/main.min.js',
    format: 'iife',
    sourceMap: process.env.NODE_ENV === 'production' ? false : 'inline',
    external,
    globals: {
        react: 'React',
        'react-dom': 'ReactDOM',
    },
    moduleName: 'contribApp',
    plugins: [
        postcss({
            extensions: ['.css'],
            plugins: [
                cssimport(),
                simplevars(),
                nested(),
                cssnext({ warnForDuplicates: false }),
                cssnano(),
                postcssModules({
                    getJSON(id, exportTokens) {
                        cssExportMap[id] = exportTokens;
                    },
                }),
            ],
            getExport(id) {
                return cssExportMap[id];
            },
        }),
        eslint({
            exclude: [
                'src/styles/**',
            ],
        }),
        babel({
            exclude: [
                'node_modules/**',
            ],
        }),
        replace({
            exclude: 'node_modules/**',
            ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        }),
        (process.env.NODE_ENV === 'production' && uglify()),
    ],
};
