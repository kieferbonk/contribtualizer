var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
var EC = protractor.ExpectedConditions;

chai.use(chaiAsPromised);
var expect = chai.expect;

describe('the app', function () {
    this.timeout(15000);

    it('has correct title', function() {
        browser.get(browser.baseUrl);
        expect(browser.getTitle()).to.eventually.match(/:: contribtualizer ::/);
    });

    it('has zero results on incorrect input', function() {
        var input = $('#input-submit');
        var errorCont = $('#error');
        input.clear();
        input.sendKeys('f aweopk o');
        input.sendKeys(protractor.Key.ENTER);
        browser.wait(EC.presenceOf(errorCont), 12000);
        expect(errorCont.isPresent()).to.eventually.be.true;
    });

    it('has 10+ results on correct input', function() {
        var input = $('#input-submit');
        var listing = $('#listing-repo');
        input.clear();
        input.sendKeys('reactjs');
        input.sendKeys(protractor.Key.ENTER);
        browser.wait(EC.presenceOf(listing), 12000);
        expect(listing.$$('li').count()).to.eventually.be.above(10);
    });

    it('displays contributor card', function() {
        var repo = $('#listing-repo').$$('li').get(0);
        var button = repo.$('button');
        var card = $('#listing-contrib');
        var graph = card.$$('li');
        button.click();
        browser.wait(EC.presenceOf(card), 12000);
        expect(graph.count()).to.eventually.be.above(0);
    });

    it('has first contributor crowned', function() {
        var contributor = $('#listing-contrib').$$('li').get(0).$('svg');
        expect(contributor.isPresent()).to.eventually.be.true;
    });
});
