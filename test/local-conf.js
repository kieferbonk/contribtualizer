exports.config = {
    baseUrl: 'http://localhost:3003',
    getPageTimeout: 160000,
    allScriptsTimeout: 170000,
    suites: {
        ros: 'ros-spec.js'
    },
    framework: 'mocha',
    maxSessions: 1,
    capabilities: {
        browserName: 'chrome'
    },
    onPrepare: function () {
        browser.ignoreSynchronization = true;
    }
};
