export default {
    github: {
        headers: {
            Accept: 'application/vnd.github.v3+json',
        },
        root: 'https://api.github.com',
        orgs: '/orgs/:org/repos',
        contributors: '/repos/:owner/:repo/contributors',
    },
};
