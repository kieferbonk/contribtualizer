import React from 'react';
import SvgIcon from './SvgIcon';
import Styles from '../../styles/OrgSelector.css';

class OrgSelector extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.org || '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.input.focus();

        if (this.props.org) this.props.callback(this.props.org);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.org !== this.props.org) {
            this.setState({
                value: newProps.org || '',
            });
        }
    }

    handleChange(e) {
        this.setState({
            value: e.target.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.input.value) return;
        this.props.callback(this.input.value);
    }

    render() {
        const buttonStyle = this.input && (this.input.value === this.props.org)
            ? `${Styles['btn-submit']} ${Styles['btn-inactive']}`
            : Styles['btn-submit'];

        return (
            <form
              className={Styles['selector-project']}
              onSubmit={this.handleSubmit}
              ref={(c) => { this.form = c; }}
            >
                <input
                  id="input-submit"
                  placeholder="github organisation"
                  type="text"
                  ref={(c) => { this.input = c; }}
                  value={this.state.value}
                  onChange={this.handleChange}
                />
                <button className={buttonStyle} type="submit">
                    <SvgIcon type="search" />
                </button>
            </form>
        );
    }
}

OrgSelector.propTypes = {
    callback: React.PropTypes.func,
    org: React.PropTypes.string,
};

export default OrgSelector;
