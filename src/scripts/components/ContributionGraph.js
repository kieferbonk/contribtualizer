import React from 'react';
import Styles from '../../styles/ContributionGraph.css';

function ContributionGraph(props) {
    return (
        <div className={Styles['graph-contrib']} style={{ width: `${props.percent}%` }} />
    );
}

ContributionGraph.propTypes = {
    percent: React.PropTypes.number.isRequired,
};

export default ContributionGraph;
