import React from 'react';
import ContributionGraph from './ContributionGraph';
import SvgIcon from './SvgIcon';
import Styles from '../../styles/ContributorListing.css';

class ContributorListing extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (nextProps.contributors.length) {
            document.body.classList.add('scroll-no');
            document.body.addEventListener('click', this.props.closeModal);
        }
    }

    render() {
        const topScore = this.props.contributors.length
            ? this.props.contributors[0].contributions
            : undefined;

        return this.props.contributors.length ? (
            <ul className={Styles['listing-contrib']} id="listing-contrib">
                <button className={Styles['modal-close']} data-action="close-modal">
                    <SvgIcon type="cross" />
                </button>
                <h3 className={Styles['header-modal']}>
                    {this.props.owner}/{this.props.repo}
                </h3>
                {this.props.contributors.map((n, k) =>
                    <li key={k}>
                        <a href={n.html_url}>
                            <img src={n.avatar_url} alt={n.login} className={Styles.avatar} />
                            {n.contributions === topScore ? <SvgIcon type="crown" className={Styles['crown-icon']} /> : null}
                            <div className={Styles['graph-container']}>
                                <ContributionGraph percent={(n.contributions / topScore) * 100} />
                                <span>{n.login} ({n.contributions})</span>
                            </div>
                        </a>
                    </li>,
                )}
            </ul>
        ) : null;
    }
}

ContributorListing.propTypes = {
    contributors: React.PropTypes.arrayOf(
        React.PropTypes.object,
    ),
    closeModal: React.PropTypes.func.isRequired,
    owner: React.PropTypes.string,
    repo: React.PropTypes.string,
};

export default ContributorListing;
