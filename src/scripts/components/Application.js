import React from 'react';
import apiConfig from '../config/config.api';
import APIUtils from '../utils/APIUtils';
import Header from './Header';
import OrgSelector from './OrgSelector';
import RepoListing from './RepoListing';
import ContributorListing from './ContributorListing';
import Styles from '../../styles/main.css';

class Application extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            org: this.props.path,
            repos: [],
            contributors: [],
            error: false,
            repo: undefined,
            owner: undefined,
            loading: false,
        };

        this.handleError = this.handleError.bind(this);
        this.storeRepos = this.storeRepos.bind(this);
        this.storeContributors = this.storeContributors.bind(this);
        this.fetchRepos = this.fetchRepos.bind(this);
        this.fetchContributors = this.fetchContributors.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    componentWillReceiveProps(newProps) {
        this.fetchRepos(newProps.path);
    }

    handleError() {
        this.setState({
            org: undefined,
            repos: [],
            contributors: [],
            error: true,
            repo: undefined,
            owner: undefined,
            loading: false,
        });
    }

    storeRepos(data) {
        const repos = data.map(n => ({
            name: n.name,
            owner: n.owner.login,
            fork: n.fork,
        }));
        this.setState({
            repos: repos.sort((a, b) => a.name.localeCompare(b.name)),
            error: false,
            loading: false,
        });
    }

    storeContributors(data) {
        this.setState({
            contributors: data.map(n => n),
            loading: false,
        });
    }

    fetchRepos(org) {
        this.setState({
            org,
            loading: true,
        });
        document.location.hash = org;

        if (org) {
            const url = apiConfig.github.root + apiConfig.github.orgs.replace(':org', org);
            APIUtils.loadJSON(url, this.storeRepos, this.handleError);
        } else {
            this.handleError();
        }
    }

    fetchContributors(repo) {
        const url = apiConfig.github.root + apiConfig.github.contributors
            .replace(':owner', repo.owner)
            .replace(':repo', repo.name);
        APIUtils.loadJSON(url, this.storeContributors, () => {});
        this.setState({
            repo: repo.name,
            owner: repo.owner,
        });
    }

    closeModal(e) {
        e.stopPropagation();

        let found = false;
        let el = e.target;

        while (el.parentNode) {
            el = el.parentNode;
            if (el === this.modalRoot) found = true;
        }

        if (!found || e.target.getAttribute('data-action') === 'close-modal') {
            e.preventDefault();
            e.stopPropagation();
            document.body.classList.remove('scroll-no');
            document.body.removeEventListener('click', this.closeModal);
            this.setState({
                contributors: [],
            });
        }
    }

    render() {
        return (
            <div className={Styles['container-main']}>
                <Header />
                <OrgSelector org={this.state.org} callback={this.fetchRepos} />
                <RepoListing
                  repos={this.state.repos}
                  loading={this.state.loading}
                  callback={this.fetchContributors}
                />
                <div ref={(c) => { this.modalRoot = c; }}>
                    <ContributorListing
                      contributors={this.state.contributors}
                      repo={this.state.repo}
                      owner={this.state.owner}
                      closeModal={this.closeModal}
                    />
                </div>
                {this.state.error && !this.state.loading
                    ? <div className={Styles['container-error']} id="error">zero results</div>
                    : null
                }
            </div>
        );
    }
}

Application.propTypes = {
    path: React.PropTypes.string,
};

export default Application;
