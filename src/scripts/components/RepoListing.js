import React from 'react';
import SvgIcon from './SvgIcon';
import Styles from '../../styles/RepoListing.css';

function RepoListing(props) {
    const spinner = props.loading
        ? <SvgIcon type="spinner" />
        : null;

    return (props.repos.length) ? (
        <ul className={Styles['listing-repo']} id="listing-repo">
            {props.repos.map((n, k) =>
                <li key={k}>
                    <button onClick={() => { props.callback(n); }}>
                        {n.name}
                        {n.fork
                            ? <div className={Styles['repo-fork']}>
                                <SvgIcon type="fork" className={Styles['fork-icon']} />
                            </div>
                            : null
                        }
                    </button>
                </li>,
            )}
        </ul>
    ) : spinner;
}

RepoListing.propTypes = {
    repos: React.PropTypes.arrayOf(
        React.PropTypes.object,
    ),
    loading: React.PropTypes.bool,
};

export default RepoListing;
