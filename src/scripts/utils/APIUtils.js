import apiConfig from '../config/config.api';

export default {
    loadJSON(url, resolve, reject) {
        fetch(url, {
            headers: apiConfig.github.headers,
        })
            .then((response) => {
                if (response.status !== 200) {
                    throw new TypeError(`Response from ${url} had an invalid status code`);
                } else if (response.headers.get('content-type').indexOf('application/json') !== -1) {
                    return response.json();
                }

                throw new TypeError(`Response from ${url} has unexpected "content-type"`);
            })
            .then((data) => {
                resolve(data);
            })
            .catch((error) => {
                /* eslint no-console: 0 */
                console.error(error.message);
                reject();
            });
    },
};
