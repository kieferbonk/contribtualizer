import React from 'react';
import ReactDOM from 'react-dom';
import Application from './components/Application';
import '../styles/main.css';

let path = document.location.hash
    ? document.location.hash.substring(1)
    : undefined;

const renderApp = () => {
    ReactDOM.render(
        <Application path={path} />,
        document.getElementById('root'),
    );
};

window.setInterval(() => {
    if (path !== document.location.hash.substring(1)) {
        path = document.location.hash.substring(1);
        renderApp();
    }
}, 200);

renderApp();

if (ENV !== 'production') {
    document.write(
        `<script src="http://${(location.host || 'localhost').split(':')[0]}:35729/livereload.js?snipver=1"></script>`,
    );
}
