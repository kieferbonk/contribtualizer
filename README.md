## installation
```
npm i
```

## developing
```
npm run watch
```

## deploying
```
NODE_ENV=production npm run deploy
```

## testing
```
npm run watch &
npm run test
```
